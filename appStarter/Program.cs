﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace appStarter
{
    class Program
    {
        public class DisposableList<T> : List<T>, IDisposable where T : IDisposable
        {
            public void Dispose()
            {
                foreach (var item in this)
                {
                    item.Dispose();
                }
                Clear(); // Optionally clear the list after disposing
            }
        }
        public class LogPath : IDisposable
        {
            public string PathEnable { get; }
            public string PathCheckRun { get; }
            public string PathAppName { get; }
            public string PathLog { get; }
            public string PathDaysToKeep { get; }
            public string Path { get; }


            public LogPath(string pathEnable, string pathCheckRun, string pathAppName, string pathLog, string pathDaysToKeep, string path)
            {
                PathEnable = pathEnable;
                PathAppName = pathAppName;
                PathLog = pathLog;
                PathDaysToKeep = pathDaysToKeep;
                PathCheckRun = pathCheckRun;
                Path = path;
            }

            public void Dispose()
            {
                // Implement any cleanup logic if needed
                // For example, releasing unmanaged resources, etc.
            }
        }
        //private static DateTime lastTime1;
        //private static TimeSpan lastTotalProcessorTime1;
        //private static DateTime curTime1;
        //private static TimeSpan curTotalProcessorTime1;
        //private static bool found = false;
        //private static string sqlInstance = "";

        //private static DateTime lastTime2;
        //private static TimeSpan lastTotalProcessorTime2;
        //private static DateTime curTime2;
        //private static TimeSpan curTotalProcessorTime2;

        //private static DateTime lastTime3;
        //private static TimeSpan lastTotalProcessorTime3;
        //private static DateTime curTime3;
        //private static TimeSpan curTotalProcessorTime3;

        //private static DateTime lastTime4;
        //private static TimeSpan lastTotalProcessorTime4; 
        //private static DateTime curTime4;
        //private static TimeSpan curTotalProcessorTime4;

        //private static DateTime lastTime5;
        //private static TimeSpan lastTotalProcessorTime5;
        //private static DateTime curTime5;
        //private static TimeSpan curTotalProcessorTime5;

        //private static DateTime lastTime6;
        //private static TimeSpan lastTotalProcessorTime6;
        //private static DateTime curTime6;
        //private static TimeSpan curTotalProcessorTime6;

        //private static long checkUsageCount = long.Parse(ConfigurationManager.AppSettings["checkUsageCount"].ToString());
        private static Boolean verifyRunOnce = true;
        private static Boolean isCheckLogsOnceWhenStartup = true;
        private static long checkRunningCount = 0;
        private static Boolean checkDatePass = false;
        //private static string resLogPath = AppDomain.CurrentDomain.BaseDirectory + "resourcesLogCSV";

        //private static int allCount = 0;
        private static DateTime lastTriggered = DateTime.MinValue; // Initialize to a minimum date
        //private static long checkDeleteLog = 300;
        private static int limit = 0; 

        public static void Main(string[] args)
        {
            //max limit 20 services
            for (int i = 1; i <= 20; i++)
            {
                if (ConfigurationManager.AppSettings[$"path{i}Enable"] == null || string.IsNullOrEmpty(ConfigurationManager.AppSettings[$"path{i}Enable"].ToString()))
                {
                    i = 21;
                }
                else
                {
                    limit += 1;
                }
            }

            bool allServicesRunning;
            
            string configuredCheckTime = ConfigurationManager.AppSettings["LogTriggerTime"];
            TimeSpan triggerTime;
            if (!TimeSpan.TryParse(configuredCheckTime, out triggerTime) && string.IsNullOrEmpty(configuredCheckTime))
            {
                SaveActionLogFile($"Invalid time format in LogTriggerTime configuration. Defaulting to 00:00:00.","",true);
                triggerTime = new TimeSpan(0, 0, 0); // Set to 12 AM (midnight)
            }
            else
            {
                SaveActionLogFile($"Time set to check and delete logs: {configuredCheckTime}.", "", true);
            }

            while (true)
            {
                try
                {
                    if (checkRunningCount == 0)
                    {
                        SaveActionLogFile("Service starter service is running", "", true);
                    }
                    using (var paths = new DisposableList<LogPath>())
                    {
                        for (int i = 1; i <= limit; i++)
                        {
                            paths.Add(new LogPath(ConfigurationManager.AppSettings[$"path{i}Enable"].ToString(),
                                ConfigurationManager.AppSettings[$"path{i}CheckRun"].ToString(),
                                ConfigurationManager.AppSettings[$"path{i}AppName"].ToString(),
                                ConfigurationManager.AppSettings[$"path{i}Log"].ToString(),
                                ConfigurationManager.AppSettings[$"path{i}DaysToKeepLog"].ToString(),
                                ConfigurationManager.AppSettings[$"path{i}"].ToString()));
                        }
                        

                        foreach (var path in paths)
                        {
                            CheckAndRunApp(path.PathEnable, path.PathCheckRun, path.PathAppName, path.Path);
                        }

                        allServicesRunning = true;

                        foreach (var path in paths)
                        {
                            if (path.PathEnable == "1" && path.PathCheckRun == "1" && !IsServiceRunning(path.PathAppName))
                            {
                                //SaveActionLogFile(path.PathAppName + " true", "", true);
                                allServicesRunning = false;
                            }
                        }

                        if (allServicesRunning == false)
                        {
                            SaveActionLogFile("One or more services are not running", "", true);
                        }
                        else
                        {
                            if (DateTime.Now.TimeOfDay >= triggerTime && DateTime.Now.Date > lastTriggered.Date)
                            {
                                checkDatePass = true;
                            }
                                
                            if (verifyRunOnce == true)
                            {
                                SaveActionLogFile("All services have been verified and are currently running", "", true);
                                verifyRunOnce = false;
                            }

                            // Identify which path contains "skynet" and process it asynchronously
                            LogPath skynetPath = paths.FirstOrDefault(p => p.PathLog.ToLower().Contains("skynet")|| p.PathLog.ToLower().Contains("arpnet"));
                            if (skynetPath != null)
                            {
                                if (isCheckLogsOnceWhenStartup)
                                {
                                    DeleteSkynetOldLogs(skynetPath.PathEnable, skynetPath.PathAppName, skynetPath.PathLog, skynetPath.PathDaysToKeep);
                                }
                                else
                                {
                                    if (checkDatePass == true)
                                    {
                                        DeleteSkynetOldLogs(skynetPath.PathEnable, skynetPath.PathAppName, skynetPath.PathLog, skynetPath.PathDaysToKeep);
                                    }
                                }

                            }
                            // Process the remaining paths synchronously
                            foreach (LogPath path in paths.Where(p => p != skynetPath))
                            {
                                if (isCheckLogsOnceWhenStartup)
                                {
                                    DeleteOldLogsFolder(path.PathEnable, path.PathAppName, path.PathLog, path.PathDaysToKeep);
                                }
                                else
                                {
                                    if (checkDatePass == true)
                                    {
                                        DeleteOldLogsFolder(path.PathEnable, path.PathAppName, path.PathLog, path.PathDaysToKeep);
                                    }
                                }
                            }

                            if (checkDatePass == true)
                            {
                                checkDatePass = false;
                                lastTriggered = DateTime.Now;
                            }
                        }
                    }
                    isCheckLogsOnceWhenStartup = false;
                }
                catch (Exception ex)
                {
                    SaveActionLogFile("General error. Error: " + ex.Message.ToString(), "", true);
                }

                //checkUsageCount++;

                Thread.Sleep(1000);


                checkRunningCount++;

                if (checkRunningCount >= 10)
                {
                    checkRunningCount = 0;
                }

                //if (checkDeleteLog >= 300)
                //{
                //    checkDeleteLog = 0;
                //}             
                
                //checkDeleteLog++;
                //SaveActionLogFile("checkDeleteLog " + checkDeleteLog, "", true);
                
            }
        }
       
        static bool IsServiceRunning(string appName)
        {
            return Process.GetProcessesByName(appName).Length > 0;
        }

        static void CheckAndRunApp(string isPathEnable, string checkPathRun, string pathAppName, string path)
        {
            if (isPathEnable == "1" && checkPathRun =="1")
            {
                if (Process.GetProcessesByName(pathAppName).Length == 0)
                {
                    SaveActionLogFile($"{pathAppName} not running. Run It.", "", true);
                    Process.Start(path);
                }     
            }
        }

        static void DeleteSkynetOldLogs(string isPathEnable, string pathAppName, string pathLog, string pathDaysToKeepLog)
        {
            if (isPathEnable == "1" && pathDaysToKeepLog != "0")
            {
                SaveActionLogFile($"Starting to delete log for {pathAppName}", "", true);
                try
                {
                    int count = 0;
                    foreach (string tempFile in Directory.EnumerateFiles(pathLog, "*.*", SearchOption.AllDirectories))
                    {
                        count++;
                        try
                        {
                            // Get the creation time of the file
                            DateTime dt = File.GetCreationTime(tempFile);
                            TimeSpan ts = DateTime.Now.Subtract(dt);
                            double numberOfDays = ts.TotalDays;
                            double tempDays = 30;

                            try
                            {
                                tempDays = double.Parse(pathDaysToKeepLog);
                            }
                            catch (Exception ex)
                            {
                            }

                            // Check if the file is older than the specified number of days
                            if (numberOfDays > tempDays)
                            {
                                // Delete the file
                                File.Delete(tempFile);
                                //SaveActionLogFile($"Deleted Log File: {tempFile}", "", true);

                                // Get the directory that contains the file
                                string parentDirectory = Directory.GetParent(tempFile).FullName;

                                // Check if the directory is empty after deleting the file
                                if (Directory.GetFiles(parentDirectory).Length == 0 && Directory.GetDirectories(parentDirectory).Length == 0)
                                {
                                    // If the directory is empty, delete it
                                    Directory.Delete(parentDirectory);
                                    //SaveActionLogFile($"Deleted Empty Directory: {parentDirectory}", "", true);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            SaveActionLogFile($"ERROR deleting Log File {tempFile}: {ex.Message}", "", true);
                        }
                        finally
                        {
                            if (count % 100 == 1)
                            {
                                Thread.Sleep(5);
                            }
                        }
                    }

                }
                catch (UnauthorizedAccessException ex)
                {
                    SaveActionLogFile($"ERROR accessing directory: {ex.Message}", "", true);
                }
                catch (Exception ex)
                {
                    SaveActionLogFile($"Unexpected error: {ex.Message}", "", true);
                }
                SaveActionLogFile("Completed to delete log for " + pathAppName, "", true);

            }
        }
        static void DeleteOldLogsFolder(string isPathEnable, string pathAppName, string pathLog, string pathDaysToKeepLog)
        {
            if (isPathEnable == "1" && pathDaysToKeepLog != "0")
            {
                SaveActionLogFile($"Starting to delete log for {pathAppName}", "", true);
                try
                {
                    int count = 0;
                    // Enumerate through files
                    foreach (string tempFile in Directory.EnumerateFiles(pathLog, "*.*", SearchOption.AllDirectories))
                    {
                        count++;
                        try
                        {
                            DateTime dt = File.GetCreationTime(tempFile);
                            TimeSpan ts = DateTime.Now.Subtract(dt);
                            double numberOfDays = ts.TotalDays;
                            double tempDays = 30;

                            try
                            {
                                tempDays = double.Parse(pathDaysToKeepLog);
                            }
                            catch (Exception ex)
                            {
                                //SaveActionLogFile("PathDatsToKeepLog config error: " + ex.Message, "", true);
                            }

                            if (numberOfDays > tempDays)
                            {
                                File.Delete(tempFile);
                            }
                        }
                        catch (Exception ex)
                        {
                            SaveActionLogFile($"ERROR deleting Log File {tempFile}: {ex.Message}", "", true);
                        }
                        finally
                        {
                            if (count % 100 == 1)
                            {
                                Thread.Sleep(5);
                            }
                        }
                    }
                }
                catch (UnauthorizedAccessException ex)
                {
                    SaveActionLogFile($"ERROR accessing directory: {ex.Message}", "", true);
                }
                catch (Exception ex)
                {
                    SaveActionLogFile($"Unexpected error: {ex.Message}", "", true);
                }


                //string[] files = Directory.GetFiles(pathLog, "*.*", SearchOption.AllDirectories);
                //SaveActionLogFile("Files gotten", "", true);
                //foreach (string tempFile in files)
                //{
                //    try
                //    {
                //        DateTime dt = File.GetCreationTime(tempFile);

                //        TimeSpan ts = DateTime.Now.Subtract(dt);

                //        double NumberOfDays = ts.TotalDays;

                //        double tempDays = 30;

                //        try
                //        {
                //            tempDays = double.Parse(pathDaysToKeepLog);
                //        }
                //        catch (Exception ex)
                //        {
                //            SaveActionLogFile("PathDatsToKeepLog config error: " + ex.Message, "", true);
                //        }

                //        if (NumberOfDays > tempDays)
                //        {
                //            File.Delete(tempFile);
                //        }
                //    }
                //    catch (Exception ex)
                //    {
                //        SaveActionLogFile("ERROR deleting Log File " + tempFile + ": " + ex.Message, "", true);
                //    }
                //}
                SaveActionLogFile("Completed to delete log for " + pathAppName, "", true);
            }
        }

        static object getCPUCounter()
        {
            PerformanceCounter cpuCounter = new PerformanceCounter();
            cpuCounter.CategoryName = "Processor";
            cpuCounter.CounterName = "% Processor Time";
            cpuCounter.InstanceName = "_Total";

            // will always start at 0
            dynamic firstValue = cpuCounter.NextValue();
            System.Threading.Thread.Sleep(1000);
            // now matches task manager reading
            dynamic secondValue = cpuCounter.NextValue();

            return secondValue;

        }

        static void SaveActionLogFile(string strData, string machineID, bool isConsole, string ErrInfo = "")
        {
            StreamWriter objReader = default(StreamWriter);
            string FileName = null;
            string appName = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;

            try
            {
                string dir = AppDomain.CurrentDomain.BaseDirectory + "\\" + appName + "ActionLog\\" + machineID;
                if (!Directory.Exists(dir))
                {
                    Directory.CreateDirectory(dir);
                }

                if (machineID == "")
                {
                    FileName = dir + "\\" + appName + "GeneralAction_" + DateTime.Now.ToString("yyyyMMdd") + ".Log";
                }
                else
                {
                    FileName = dir + "\\" + appName + "Action" + machineID + "_" + DateTime.Now.ToString("yyyyMMdd") + ".Log";
                }

                if (System.IO.File.Exists(FileName) == true)
                {
                    objReader = new StreamWriter(FileName, true);
                }
                else
                {
                    objReader = new StreamWriter(FileName, false);
                }

                string[] files = Directory.GetFiles(dir);

                foreach (string tempFile in files)
                {
                    DateTime dt = File.GetCreationTime(tempFile);

                    TimeSpan ts = DateTime.Now.Subtract(dt);

                    double NumberOfDays = ts.TotalDays;

                    double tempDays = 30;

                    if (NumberOfDays > tempDays)
                    {
                        File.Delete(tempFile);
                    }
                }
                objReader.WriteLine((DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt")) + " : " + strData);

                if (isConsole)
                {
                    Console.WriteLine((DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt")) + " : " + strData);
                }

                objReader.WriteLine();
                objReader.Close();
            }
            catch (Exception Ex)
            {
                ErrInfo = Ex.Message;
            }
        }

    }
}
